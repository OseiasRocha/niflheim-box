/*****************************************************************************
  Arduino sketch to report the temperature using an ESP32 with connected
  Dallas DS18B20 sensor, for the Blynk system.
  http://www.blynk.cc
*****************************************************************************/

/*
Copyright (c) 2017 Scott Allen

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#define BLYNK_DEBUG
#define BLYNK_PRINT Serial // Comment this out to disable prints and save space

#include <BlynkSimpleEsp32_BLE.h>
#include <BLEDevice.h>
#include <BLEServer.h>
#include <LiquidCrystal.h>
#include <OneWire.h>
#include <DallasTemperature.h>

#define SYM_BLUETOOTH 0
#define SYM_CHECK 1
#define SYM_CROSS 2

#define SPEAKER_PIN 23
#define LDR_PIN A0

int freq = 2000;
int channel = 0;
int resolution = 10;

byte bluetooth[8] = {
  B00110,
  B10101,
  B01110,
  B00100,
  B01110,
  B10101,
  B00110,
};

byte check[8] = {
  B00000,
  B00001,
  B00011,
  B10110,
  B11100,
  B01000,
  B00000,
};

byte cross[8] = {
  B00000,
  B11011,
  B01110,
  B00100,
  B01110,
  B11011,
  B00000,
};

// You should get Auth Token in the Blynk App.
// Go to the Project Settings (nut icon).
char auth[] = "6b590e4944b1474f84cac57ed90a63d5";

// Temperature sensor full ID, including family code and CRC
DeviceAddress tempSensor = { 0x28, 0xFF, 0xC3, 0x6B, 0xA1, 0x16, 0x04, 0x5A };

LiquidCrystal lcd(26, 25, 17, 16, 27, 14);

// Alert messages
char alertMessageLow[] = "Box temperature is LOW!";
char alertMessageHigh[] = "Box temperature is HIGH!";

// Pin used for the OneWire interface
#define ONEWIRE_PIN 0

OneWire oneWire(ONEWIRE_PIN);
DallasTemperature sensors(&oneWire);
SimpleTimer timer;

// Current temperature is a global so it can used to reset min/max temperatures
float currentTemp = 0;

// Minimum and maximum temperatures are set to out of range values that will be
// overridden the first time the actual temperature is read.
float minTemp = 10000.0;
float maxTemp = -10000.0;

int alertTempLow, alertTempHigh;
boolean lowAlertOn = false;
boolean highAlertOn = false;
int ldr = 0;
int timer1 = 0;


// Virtual pins to send temperature reading (from device)
#define V_PIN_TEMP_A V0 // Value - Widget must allow color change
#define V_PIN_TEMP_B V1
// Virtual pins for alert temperature set points (to device)
#define V_PIN_ALERT_LOW V2
#define V_PIN_ALERT_HIGH V3
// Virtual pins for min/max temperatures (from device)
#define V_PIN_TEMP_MIN V4
#define V_PIN_TEMP_MAX V5
// Virtual pin for min/max temperature reset (to device)
#define V_PIN_MIN_MAX_RESET V6

// Hysteresis in degrees for alerts
#define ALERT_HYSTERESIS 1

// Temperature reading interval, in SECONDS
#define READ_INTERVAL 5

// Check interval for network connection, in milliseconds
// This should be a bit shorter than the Blynk reconnect poll time, which
// at the time of writing was 7 seconds
#define CONNECTION_CHECK_TIME 6500

// LED flash time for link down, in milliseconds
#define LED_DISC_FLASH_TIME 2000

// LED blink rate for "sensor not found" indication, in milliseconds
#define NO_SENSOR_BLINK_SPEED 500

// LED blink time for "sensor not found" indication, in milliseconds
#define NO_SENSOR_BLINK_TIME 200

// LED flash time for successful sensor read, in milliseconds
#define LED_READ_OK_FLASH_TIME 30

// LED flash time for sensor read error, in milliseconds
#define LED_READ_ERR_FLASH_TIME 5000

// Pin for LED indicator
#define LED_PIN 2

// The number of bits of temperature resolution
// 9 = 0.5, 10 = 0.25, 11 = 0.125, 12 = 0.0625 degrees Celsius
#define TEMPERATURE_RES_BITS 10

// Temperature conversion wait time, in milliseconds
#define READ_CONV_WAIT 800

// Pin values for indicator LED on and off
#define LED_ON LOW
#define LED_OFF HIGH

// Widget colors for alerts
#define ALERT_COLOR_OK   "#23C48E" // Green
#define ALERT_COLOR_LOW  "#5F7CD8" // Dark Blue
#define ALERT_COLOR_HIGH "#D3435C" // Red


//--------------------- SETUP -------------------
void setup() {
  Serial.begin(9600);

  Serial.println("Waiting for connections...");

  Blynk.setDeviceName("NIFLHEIM BOX");

  Blynk.begin(auth);

  ledcSetup(channel, freq, resolution);
  ledcAttachPin(SPEAKER_PIN, channel);
  
  pinMode(LED_PIN, OUTPUT);
  indLEDoff();
  
  sensors.begin();
  if (!sensors.isConnected(tempSensor)) {
    while (true) {
      indLEDon();
      delay(NO_SENSOR_BLINK_TIME);
      indLEDoff();
      delay(NO_SENSOR_BLINK_SPEED - NO_SENSOR_BLINK_TIME);
    }
  }

  // Set the resolution of the temperature readings
  sensors.setResolution(tempSensor, TEMPERATURE_RES_BITS);

  // We'll do the "wait for conversion" ourselves using a timer,
  // to avoid the call to delay() that the library would use
  sensors.setWaitForConversion(false);

    lcd.createChar(0, bluetooth);
    lcd.createChar(1, check);
    lcd.createChar(2, cross);
    lcd.begin(16, 2);
    lcd.clear();
    lcd.print("NIFLHEIM BOX");

 
    // go to row 1 column 0, note that this is indexed at 0
    lcd.setCursor(14,0); 
    lcd.write(byte(SYM_BLUETOOTH));
    display_disconnected();
    display_closed();
    
  // Start the interval timer that checks if the connection is up
  timer.setInterval(CONNECTION_CHECK_TIME, connectionCheck);

  // Start the timer that handles the temperature read interval
  timer.setInterval(READ_INTERVAL * 1000, startSensorRead);
}
//-----------------------------------------------

// Synchronize pins when connection comes up
BLYNK_CONNECTED() {
  Blynk.syncAll();
}

// Set low alert temperature
BLYNK_WRITE(V_PIN_ALERT_LOW) {
  alertTempLow = param.asInt();
}

// Set high alert temperature
BLYNK_WRITE(V_PIN_ALERT_HIGH) {
  alertTempHigh = param.asInt();
}

// Reset minimum and maximum temperatures
BLYNK_WRITE(V_PIN_MIN_MAX_RESET) {
  if (param.asInt()) { // if button pressed
    minTemp = maxTemp = currentTemp;
    Blynk.virtualWrite(V_PIN_TEMP_MIN, currentTemp);
    Blynk.virtualWrite(V_PIN_TEMP_MAX, currentTemp);
  }
}

// Blink the LED if the connection is down
void connectionCheck() {
  if (Blynk.connected()) {
    return;
  }

  indLEDon();
  delay(LED_DISC_FLASH_TIME);
  indLEDoff();
}

// Start a temperature read and the conversion wait timer
void startSensorRead() {
  if (!Blynk.connected()) {
    return;
  }

  sensors.requestTemperatures();
  timer.setTimeout(READ_CONV_WAIT, sensorRead);
}

// Read the temperature from the sensor and perform the appropriate actions
void sensorRead() {
  int16_t tempRaw;

  Serial.println("SENSOR READ");
  
  if (!Blynk.connected()) {
    return;
  }

  tempRaw = sensors.getTemp(tempSensor);
  
  if (tempRaw != DEVICE_DISCONNECTED_RAW) {

    currentTemp = sensors.rawToCelsius(tempRaw);

    display_temperature(currentTemp);
    
    // Send temperature value
    Blynk.virtualWrite(V_PIN_TEMP_A, currentTemp);
    Blynk.virtualWrite(V_PIN_TEMP_B, currentTemp);

    // Low temperature alerts
    if ((currentTemp <= alertTempLow) && !lowAlertOn) {
      Blynk.setProperty(V_PIN_TEMP_A, "color", ALERT_COLOR_LOW);
      Blynk.notify(alertMessageLow);
      lowAlertOn = true;
    }
    else if (lowAlertOn && (currentTemp > alertTempLow + ALERT_HYSTERESIS)) {
      Blynk.setProperty(V_PIN_TEMP_A, "color", ALERT_COLOR_OK);
      lowAlertOn = false;
    }

    // High temperature alerts
    if ((currentTemp >= alertTempHigh) && !highAlertOn) {
      Blynk.setProperty(V_PIN_TEMP_A, "color", ALERT_COLOR_HIGH);
      Blynk.notify(alertMessageHigh);
      highAlertOn = true;
    }
    else if (highAlertOn && (currentTemp < alertTempHigh - ALERT_HYSTERESIS)) {
      Blynk.setProperty(V_PIN_TEMP_A, "color", ALERT_COLOR_OK);
      highAlertOn = false;
    }

    // Minimum and maximum temperatures
    if (currentTemp < minTemp) {
      minTemp = currentTemp;
      Blynk.virtualWrite(V_PIN_TEMP_MIN, currentTemp);
    }
    if (currentTemp > maxTemp) {
      maxTemp = currentTemp;
      Blynk.virtualWrite(V_PIN_TEMP_MAX, currentTemp);
    }

    flashLED(LED_READ_OK_FLASH_TIME);
  }
  else {
    flashLED(LED_READ_ERR_FLASH_TIME);
  }
}

//--------------------- LOOP --------------------
void loop() {
  Blynk.run();
  timer.run();
  check_connection();
  check_door();
}
//-----------------------------------------------

// Flash the indicator LED for the specified duration
void flashLED(long dur) {
  timer.setTimeout(dur, indLEDoff);
  indLEDon();
}

// Turn on the indicator LED
void indLEDon() {
  digitalWrite(LED_PIN, LED_ON);
}

// Turn off the indicator LED
void indLEDoff() {
  digitalWrite(LED_PIN, LED_OFF);
}

void check_connection() {
  if (Blynk.connected()) {
    display_connected();
  } else {
    display_disconnected();
  }
}

void display_connected() {
    //lcd.setCursor(15,0);
    //lcd.write(" ");
    lcd.setCursor(15,0);
    lcd.write(byte(SYM_CHECK));
}

void display_disconnected() {
    //lcd.setCursor(15,0);
    //lcd.write(" ");
    lcd.setCursor(15,0);
    lcd.write(byte(SYM_CROSS));
}

void display_closed() {
    lcd.setCursor(0,1);
    lcd.print("FECHADA");
}

void display_opened() {
    lcd.setCursor(0,1);
    lcd.print("ABERTA ");
}

void display_temperature(float temp) {
    lcd.setCursor(8,1);
    lcd.print(temp);
    lcd.setCursor(14,1);
    lcd.print((char)223);
    lcd.print("C");    
}

void check_door() {
  ldr = analogRead(LDR_PIN);
  
  if(ldr >= 500){

     timer1++;
     delay(1000);
     Serial.println(timer1);
    
  }

  else if(ldr < 500) {
    display_closed();
    timer1 = 0;
  }

   
    while((ldr=(analogRead(LDR_PIN)) > 500) && (timer1 > 10)){

      Serial.println("TAMPA ABERTA");
      Blynk.notify("FECHE A TAMPA");
      display_opened();
      

    ledcWrite(channel, 125);
    
    for(int hz = 440; hz < 1000; hz++){
      ledcWriteTone(channel, hz);
      delay(5);
    }
  
    for(int hz = 1000; hz > 440; hz--){
      ledcWriteTone(channel, hz);
      delay(5);
    }

    if(ldr=(analogRead(LDR_PIN)) < 500) break;  

  }
  ledcWriteTone(channel, 0);
}
