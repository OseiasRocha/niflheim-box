# Niflheim Box

## Introdução

Com a crescente demanda na liberação de vacinas para regiões cada vez mais 
distantes do seu centro de desenvolvimento, um armazenamento adequado e 
controlado para as mesmas seria de essencial necessidade para um transporte
adequado, seguindo tal demanda, o projeto a ser apresentado trará um protótipo
para a implementação de uma Caixa de Vacinas que contará com um resfriamento
(visando manter a temperatura o mais adequada possível para o transporte seguro 
das vacinas), um display indicador de temperatura, contador da abertura da
tampa, uma conexão com dispositivos móveis, para controle e avisos, e um falante
interno para informar as pessoas que realizarão o transporte caso quaisquer
anomalias venham a ocorrer em meio ao transporte da mesma; Tudo visando uma 
melhor comodidade e segurança no transporte de vacinas.

## Conceitos Preliminares

Para que haja uma qualidade no mínimo satisfatória para a realização do projeto,
alguns pré-requisitos devem ser tomados, visando um transporte seguro das 
vacinas na caixa e respeitando os parâmetros de automação já estabelecidos.
A caixa deve ser de Isopor revestida em Plástico de Reina de Polietileno, para
que torne-a termica, e resistente para o transporte.
2 sensores de temperatura serão adicionados na caixa, a fim de controlar a
temperatúra média e informar em um LCD instado na mesma.
Será adicionado um contador na tampa par informar se a caixa foi a berta e se
for o caso, quantas vezes foi aberta, visando um controle mais apurado dos dados
durante o transporte.
Todos os dados recebidos pelos sensores instalados na caixa, poderão ser 
vizualizados em um smartphone pré-configurado, que receberá informações enviadas
pela caixa, de forma remota via Bluetooth.

## Especificação de Componentes

Os componentes estão Listados na [Wiki](https://gitlab.com/OseiasRocha/niflheim-box/wikis/home)

## Requisitos de Software

-> A tampa não pode permanecer mais que 2 minutos aberta, se isto ocorrer, deve 
    tocar um alarme avisando esta ocorrência.
   Quando a tampa é aberta, inicia-se uma contagem de 2 minutos, se a tampa for 
   fechada antes dos 2 minutos, ela deve seguir sua rotina normal. Se ela ficar 
   aberta por um tempo superior aos 2 minutos, ela deve enviar uma mensagem ao 
   celular cadastrado e iniciar o aviso sonoro, sendo suspenso após o fechamento 
   da tampa.

-> Quando a tampa for aberta e fechada (não importando o tempo que ficou aberta) 
    não pode ser efetuado alarme de temperatura acima do permitido logo em 
    seguida, pois a troca de temperatura com o ambiente externo irá aumentar 
    momentaneamente a temperatura interna. Desta forma, devem ser feitas 
    verificações para garantir a diminuição da temperatura para o valor 
    adequado.

-> A caixa estando fechada (respeitando o item 2), se a temperatura fora 
    superior à uma  pré-determinada , acionar alarme e enviar mensagem para o app.

-> Se houver falha  em algum componente do hardware (responsável pelas 
    verificações), deve ser enviado para o app o ocorrido e soar o alarme.

## Documentos de Referência

Tecmundo: https://www.tecmundo.com.br/cooler/825-o-que-e-o-cooler-.htm

Manual do Mundo: http://www.manualdomundo.com.br/2018/01/como-fazer-uma-geladeira-caseira-que-chega-56c-geladeira-peltier/

Arduino & Cia: https://www.arduinoecia.com.br/2014/11/cooler-arduino-pastilha-termoeletrica-tec1-12706.html

