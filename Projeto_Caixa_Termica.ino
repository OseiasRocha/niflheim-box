#include <ESP8266_Lib.h>
#include "DHT.h"

#define BLYNK_PRINT Serial
#define pinLdr A0
#define pinBuzzer 8

#include "BlynkSimpleShieldEsp8266.h"


char auth[] = "f3257044048049a38ace26ed01c0ea52";
char ssid[] = "Alisson";
char pass[] = "Qwerty2018";

#include "SoftwareSerial.h"
SoftwareSerial EspSerial(12, 13); // RX, TX

// Your ESP8266 baud rate:
#define ESP8266_BAUD 9600

ESP8266 wifi(&EspSerial);

const int pino_dht = 9; // pino onde o sensor DHT está conectado
float temperatura; // variável para armazenar o valor de temperatura
float umidade; // variável para armazenar o valor de umidade
int ldr, cont = 0, aux = 0, timer = 0, buzzer = 0;

DHT dht(pino_dht, DHT11); // define o pino e o tipo de DHT

void setup()
{
  // Debug console
  Serial.begin(9600);

  delay(10);

  // Set ESP8266 baud rate
  EspSerial.begin(ESP8266_BAUD);
  delay(10);

  Blynk.begin(auth, wifi, ssid, pass);
  
  dht.begin(); // inicializa o sensor DHT

  pinMode(pinBuzzer, OUTPUT);
}

void loop()
{
  Blynk.run();

  Serial.print("Valor LDR: ");
  Serial.print(ldr=(analogRead(pinLdr)));
  Serial.println();
  
 /* if(( ldr >= 500) && (aux == 0)){
   
   cont++;
   aux = 1;
  
   timer++;

   Serial.println(timer);
   delay(1000);
  }
   else if(ldr < 500){
      
      aux = 0;
      timer = 0;
    }
*/

  if(ldr >= 500){

     timer ++;
     delay(1000);
     Serial.println(timer);
    
  }

  else if(ldr < 500) timer = 0;

   
    while((ldr=(analogRead(pinLdr)) > 500) && (timer > 10)){

      Serial.println("TAMPA ABERTA");
      Blynk.notify("FECHE A TAMPA");
      

      digitalWrite(pinBuzzer, HIGH);
      delay(250);
      digitalWrite(pinBuzzer, LOW);
      delay(250);

      if(ldr=(analogRead(pinLdr)) < 500) break;      
      
      
    }
    
  Serial.print("A tampa foi aberta ");
  Serial.print(cont);
  Serial.print(" vez(es).");
  Serial.println();

  

  
// Aguarda alguns segundos entre uma leitura e outra
  delay(2000); // 2 segundos (Datasheet)

  // A leitura da temperatura ou umidade pode levar 250ms
  // O atraso do sensor pode chegar a 2 segundos
  temperatura = dht.readTemperature(); // lê a temperatura em Celsius
  umidade = dht.readHumidity(); // lê a umidade

    Blynk.virtualWrite(V0, temperatura);
    Blynk.virtualWrite(V1, umidade);

 // Se ocorreu alguma falha durante a leitura
  if (isnan(umidade) || isnan(temperatura)) {
    Serial.println("Falha na leitura do Sensor DHT!");
  }
  else { // Se não
    // Imprime o valor de temperatura  
    Serial.print("Temperatura: ");
    Serial.print(temperatura);
    Serial.print(" *C ");
    
    Serial.print("\t"); // tabulação
  
    // Imprime o valor de umidade
    Serial.print("Umidade: ");
    Serial.print(umidade);
    Serial.print(" %\t"); 
    
    Serial.println(); // nova linha
  }

  if(temperatura < 2){

    Serial.println("TEMPERATURA ABAIXO!!!");
    Blynk.notify("ABAIXO DE 2ºC! RETIRE GELO!");
    
  }

  else if(temperatura > 8){

    Serial.println("TEMPERATURA ACIMA!!!");
    Blynk.notify("ACIMA DE 8ºC! COLOCAR MAIS GELO!");
    
  }
  
}
